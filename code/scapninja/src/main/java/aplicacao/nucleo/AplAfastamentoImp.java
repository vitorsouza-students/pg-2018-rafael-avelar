package aplicacao.nucleo;

import java.text.DateFormat;
import java.util.List;

import com.google.inject.Inject;


import models.nucleo.Afastamento;
import models.nucleo.Onus;
import models.nucleo.Pessoa;
import models.nucleo.SituacaoSolic;
import models.nucleo.TipoAfastamento;
import ninja.jpa.UnitOfWork;
import dao.nucleo.AfastamentoDAO;
import dao.secretaria.PessoaDAO;  // REFERENTE A ENVIO DE EMAILS
//import org.apache.commons.mail.Email;
//import org.apache.commons.mail.EmailException;
//import org.apache.commons.mail.SimpleEmail;

//import br.com.caelum.vraptor.simplemail.AsyncMailer;
import com.google.inject.persist.Transactional;

import aplicacao.secretaria.AplPessoa;

public class AplAfastamentoImp implements AplAfastamento{

	@Inject
	private AfastamentoDAO afastamentoDAO;
	
	@Inject  
	private PessoaDAO pessoaDAO;
	
	@Inject
	private AplPessoa aplPessoa;
	
	
//	private final AsyncMailer mailer;
//	
//	@Inject
//	public AplAfastamentoImp(AsyncMailer mailer){
//		this.mailer = mailer;
//	}
	
	@Override
	public void salvar(Afastamento novoAfastamento,Pessoa solicitante,TipoAfastamento tipo,
			Onus onusAfastamento) {
		
		//Objeto DTO para inserir no Banco;
		Afastamento afastamentoAInserir = new Afastamento();
		
		afastamentoAInserir.setData_criacao(novoAfastamento.getData_criacao());
		afastamentoAInserir.setData_fimAfast(novoAfastamento.getData_fimAfast());
		afastamentoAInserir.setData_fimEvento(novoAfastamento.getData_fimEvento());
		afastamentoAInserir.setData_iniAfast(novoAfastamento.getData_iniAfast());
		afastamentoAInserir.setData_iniEvento(novoAfastamento.getData_iniEvento());
		afastamentoAInserir.setMotivo_afast(novoAfastamento.getMotivo_cancel());
		afastamentoAInserir.setNome_cidade(novoAfastamento.getNome_cidade());
		afastamentoAInserir.setNome_evento(novoAfastamento.getNome_evento());
		afastamentoAInserir.setOnus(novoAfastamento.getOnus());
		afastamentoAInserir.setTipoAfastamento(novoAfastamento.getTipoAfastamento());
		afastamentoAInserir.setMotivo_afast(novoAfastamento.getMotivo_afast());
		
		afastamentoAInserir.setSolicitante(solicitante);
		
		
		if(tipo.getTipoAfastamento().equals("NACIONAL")){
			afastamentoAInserir.setSituacaoSolicitacao(SituacaoSolic.LIBERADO);
			//novoAfastamento.setSituacaoSolicitacao(situacao);
		}else{
			afastamentoAInserir.setSituacaoSolicitacao(SituacaoSolic.INICIADO);
			//SituacaoSolic situacao = SituacaoSolic.INICIADO;
			//novoAfastamento.setSituacaoSolicitacao(situacao);
		}
		afastamentoDAO.salvar(afastamentoAInserir);
		
		
//		novoAfastamento.setSolicitante(solicitante);
//		novoAfastamento.setTipoAfastamento(tipo);
//		novoAfastamento.setOnus(onusAfastamento);
//		
//		Pessoa verificaPessoa;
//		
//		verificaPessoa=aplPessoa.buscaMatricula(solicitante.getMatricula());
//		
//		if(verificaPessoa==null){
//			 System.out.println("Não encountrou o solicitante");
//		}
//		if(tipo.getTipoAfastamento().equals("NACIONAL")){
//			SituacaoSolic situacao = SituacaoSolic.LIBERADO;
//			novoAfastamento.setSituacaoSolicitacao(situacao);
//		}else{
//			SituacaoSolic situacao = SituacaoSolic.INICIADO;
//			novoAfastamento.setSituacaoSolicitacao(situacao);
//		}
//		afastamentoDAO.salvar(novoAfastamento);
		
//		List<Pessoa> listaProfessores = pessoaDAO.listaProfessores();
//		DateFormat f = DateFormat.getDateInstance(DateFormat.FULL);
//		
//		for(int i=0;i<listaProfessores.size();i++){
//			Email email = new SimpleEmail();
//	        email.setSubject("SCAP - Um novo Afastamento foi solicitado");
//	        try {
//				email.addTo(listaProfessores.get(i).getEmail());
//				email.setMsg("O Professor: "+novoAfastamento.getSolicitante().getNome()+" solicitou um Afastamento a partir de: "+
//				f.format(novoAfastamento.getData_iniAfast().getTime())+". Até: "+f.format(novoAfastamento.getData_fimAfast().getTime())+".");
//				mailer.asyncSend(email);
//				
//			} catch (EmailException e) {
//				e.printStackTrace();
//			}
//		}
		
	}
	
	//@UnitOfWork
	@Override
	public List<Afastamento> listaAfastamentos() {
		return afastamentoDAO.listaAfastamentos();
	}
	
	//@UnitOfWork
	@Override
	public Afastamento buscaId(String id_afastamento) {
		return afastamentoDAO.buscaId(id_afastamento);
	}
	
	//@Transactional
	@Override
	public void mudarStatus(Afastamento afastamento, SituacaoSolic novoStatus,Pessoa logado) {
		afastamento.setSituacaoSolicitacao(novoStatus);
		afastamentoDAO.merge(afastamento);
//		Email email = new SimpleEmail();
//        email.setSubject("SCAP - O status da sua Solicitação de Afastamento foi alterado.");
//        try {
//			email.addTo(afastamento.getSolicitante().getEmail());
//			email.setMsg("O status de sua Solicitação de Afastamento ID: "+afastamento.getId_afastamento().toString()+" foi alterado para "
//							+novoStatus.getStatusAfastamento()+" por: "+logado.getNome()+" "+logado.getSobreNome()+".");
//			mailer.asyncSend(email);
//			
//		} catch (EmailException e) {
//			e.printStackTrace();
//		}
	}

}
