package aplicacao.nucleo;

import java.util.List;

import models.nucleo.Afastamento;
import models.nucleo.Onus;
import models.nucleo.Pessoa;
import models.nucleo.SituacaoSolic;
import models.nucleo.TipoAfastamento;

public interface AplAfastamento {
	
	public void salvar(Afastamento novoAfastamento,Pessoa solicitante,TipoAfastamento tipo,Onus onusAfastamento);
	
	public List<Afastamento> listaAfastamentos();
	
	public Afastamento buscaId(String id_afastamento);
	
	public void mudarStatus(Afastamento afastamento,SituacaoSolic novoStatus,Pessoa logado);
	
}
