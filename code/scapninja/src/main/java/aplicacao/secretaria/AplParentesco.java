package aplicacao.secretaria;

import models.nucleo.TipoParentesco;

public interface AplParentesco {
	
	void salvar(String matricula1,String matricula2,TipoParentesco tipo);
	
	boolean checaParentesco(String matricula1,String matricula2);
	
}
