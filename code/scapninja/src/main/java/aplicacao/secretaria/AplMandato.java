package aplicacao.secretaria;

import models.nucleo.Mandato;

public interface AplMandato {
	
	public void salvar(Mandato novoMandato,String matricula);
	
	public boolean checaMandato(String id_pessoa);
	
}
