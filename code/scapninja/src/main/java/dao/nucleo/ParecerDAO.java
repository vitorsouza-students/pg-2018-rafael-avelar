package dao.nucleo;

import java.util.List;

import models.nucleo.Parecer;

public interface ParecerDAO extends BaseDAO<Parecer> {
	
	public Parecer buscaId(String id_parecer);
	
	public List<Parecer> buscaPorAfastamento(String id_afastamento);

}
