package dao.nucleo;

import java.util.List;

import models.nucleo.Relator;

public interface RelatorDAO extends BaseDAO<Relator> {//ESSA CLASSE FOI CONSIDERADA DESNECESSÃ�RIA EM UMA REAVALIAÃ‡ÃƒO DO DIAGRAMA DE CLASSES, SEUS MÃ‰TODOS DEVEM SER MOVIDOS PARA 
	//A CLASSE AfastamentoDAO DESTE PACOTE
	
	public Relator buscaId(String id_relator);
	
	public Relator buscaPorAfastamento(String id_afastamento);
	
	public List<Relator> listaRelatores();
}
