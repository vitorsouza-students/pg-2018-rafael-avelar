package dao.nucleo;

import javax.persistence.EntityManager;
//import javax.persistence.PersistenceContext;


import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.persist.Transactional;

@Transactional
public class JPABaseDAO<T> implements BaseDAO<T>{
	
	 @Inject
	 Provider<EntityManager> entitiyManagerProvider;
	
	
	private EntityManager manager;
	
	public JPABaseDAO(){
		
	}
	
	protected EntityManager getEntityManager() {
		EntityManager manager = this.entitiyManagerProvider.get();
		
		return manager;
	}

	@Override
	@Transactional
	public void salvar(T object) {
		
		EntityManager manager = this.entitiyManagerProvider.get();
		manager.persist(object);
		
	}
	
	@Override
	@Transactional
	public T merge(T object) {			
		EntityManager manager = this.entitiyManagerProvider.get();
		manager.merge(object);
		return manager.merge(object);
		
	}
	@Transactional
	public void delete(T object) {		
		EntityManager manager = this.entitiyManagerProvider.get();
		manager.remove(manager.merge(object));
	}

}
