package dao.secretaria;

import models.nucleo.Mandato;
import dao.nucleo.BaseDAO;

public interface MandatoDAO extends BaseDAO<Mandato> {
	
	public Mandato buscaId(String id_mandato);
	
	public boolean checaMandato(String id_pessoa);
	
}
