package dao.secretaria;

import java.util.List;

import models.nucleo.Documento;
import dao.nucleo.BaseDAO;

public interface DocumentoDAO extends BaseDAO<Documento> {
	
	public Documento buscaId(String id_documento);
	
	public List<Documento> buscaPorAfastamento(String id_afastamento);
	
}
