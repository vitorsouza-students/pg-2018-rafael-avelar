# README #

Projeto de Graduação de Rafael dos Anjos Avelar: _Aplicação do método FrameWeb no
desenvolvimento de um sistema de informação utilizando o framework Ninja_

### Resumo ###

O desenvolvimento de aplicações para a Internet é um ramo muito importante, não apenas pela base de usuários, mas sim pela grande quantidade de possibilidades de soluções que ela pode prover. Foram criadas muitas tecnologias para que WebApps sejam criados com cada vez mais agilidade, confiabilidade e facilidade de uso. A área da ciência da computação não está concentrada só na criação de tecnologias, mas também em aplica-las da melhor maneira possível. Com a intenção de estudar como aplicações Web podem ser melhor desenvolvidas, surgiu a Engenharia Web.

Os artefatos tecnológicos conhecidos como Frameworks de desenvolvimento Web são o foco de estudo da metodologia FrameWeb(FrameWork-based Desing Method for Wev Engeneering), que divide tais frameworks em categorias e sugeri o uso de modelos, facilitando a utilização de tais tecnologias. O método já foi utilizando anteriormente com frameworks na plataforma Java EE, porém mais usos do método em conjunto com diferentes frameworks agregam valor ao FrameWeb.

Para testar o método com frameworks diferentes foi desenvolvida uma Web App - Sitema de Controle de Afastamento de Professores- ou SCAP. Frameworks baseados na plataforma Java EE 7 foram escolhidos, mais especificamente, o Ninja Framework, e outros que o compõem. O objetivo deste trabalho foi implementar uma nova versão do SCAP com o framework fullstack Ninja.